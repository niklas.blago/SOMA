/*
  File autogenerated by gengetopt version 2.22.6
  generated with the following command:
  gengetopt -i soma.ggo -a som_args --set-version=hash-unknown-version-hash

  The developers of gengetopt consider the fixed text that goes in all
  gengetopt output files to be in the public domain:
  we make no copyright claims on it.
*/

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef FIX_UNUSED
#define FIX_UNUSED(X) (void) (X) /* avoid warnings for unused params */
#endif

#include <getopt.h>

#include "cmdline.h"

const char *som_args_purpose = "Running SCMF simulations and timings.";

const char *som_args_usage = "Usage: ./SOMA options";

const char *som_args_versiontext = "SOMA  Copyright (C) 2016-2018 Ludwig Schneider, Ulrich Welling, Marcel\nLangenberg, Fabien Leonforte, Juan Orozco and more. This program comes with\nABSOLUTELY NO WARRANTY; see GNU Lesser General Public License v3 for details.\nThis is free software, and you are welcome to redistribute it under certain\nconditions; see GNU Lesser General Public License v3 for details. \n\n You are using SOMA please cite: \n * Schneider, Ludwig and Müller, Marcus \n \t \"Multi-Architecture Monte-Carlo (MC) Simulation of\n \t  Soft Coarse-Grained Polymeric Materials:\n \t  SOft coarse grained Monte-carlo Acceleration (SOMA)\", submitted to\nComputer Physics Communications. \n  * Daoulas, Kostas Ch. and Müller, Marcus , J. Chem.Phys.2006, 125,18";

const char *som_args_description = "";

const char *som_args_detailed_help[] = {
  "  -h, --help                    Print help and exit",
  "      --detailed-help           Print help, including all details and hidden\n                                  options, and exit",
  "  -V, --version                 Print version and exit",
  "  -c, --coord-file=filename     File containing the system description and all\n                                  Coordinates. (HDF5-Format)",
  "  -t, --timesteps=timesteps     Number of MC sweeps carried out by SOMA.",
  "  -a, --ana-file=filename       File containing the analysis frequency and is\n                                  going to be appended by the new measured\n                                  observables. (HDF5-Format)  (default=`')",
  "  The appending characteristics require, that the user makes sure, that\n  previous data also refers to the same system. Especially in the case of\n  different grid sizes this can cause issue and maybe no data is written, if\n  the dimension do not match.",
  "  -g, --gpus=gpus               Number of GPUs per MPI-node to use. The devices\n                                  0-(gpus-1) are going to be occupied by SOMA.\n                                  Every node must feature this number of nodes\n                                  and it is assumed that the scheduler assignes\n                                  the ranks consequently to the nodes. If set\n                                  to 0, SOMA tries to run on host-code. Ignored\n                                  if compiled without OPENACC.  (default=`0')",
  "  -o, --only-gpu=gpuId          Specify a specific Device for all ranks. Useful\n                                  for MPI-single rank runs. This option\n                                  overrides the --gpus option.",
  "  -s, --screen-output-interval=seconds\n                                Specify the number of seconds between an output\n                                  about timings on the screen.  (default=`10')",
  "  -r, --rng-seed=seed           Global seed for the pseudo-random number\n                                  generator. If you pass seed < 0, seed =\n                                  time(NULL) will be used. Option useful for\n                                  debuggin purposes.  (default=`-1')",
  "  -p, --pseudo-random-number-generator=PRNG\n                                Option to select the pseudo random number\n                                  generator.  (possible values=\"PCG32\",\n                                  \"MT\", \"TT800\" default=`PCG32')",
  "  -n, --omp-threads=omp_threads Number of omp threads used per MPI rank. If you\n                                  pass n < 1 it will be set to 1.\n                                  (default=`1')",
  "      --nonexact-area51         Specify to use the exact check of area51. This\n                                  includes checks, whether a particle moves\n                                  through a forbidden area51. Performance might\n                                  be slightly increased if switched to\n                                  nonexact. Configuration generation is always\n                                  in exact mode.  (default=off)",
  "      --move-type=MVT           Specify the Monte-Carlo move type.  (possible\n                                  values=\"TRIAL\", \"SMART\" default=`SMART')",
  "      --iteration-alg=ITR-ALG   Specify the iteration algorithm of the beads.\n                                  This specifies also the level of parallelism\n                                  that is possible.  (possible\n                                  values=\"POLYMER\", \"SET\"\n                                  default=`POLYMER')",
  "      --skip-tests              Skip tests SOMA is usually preforming before\n                                  and after the simulation to ensure integrety\n                                  of the data.  (default=off)",
  "  -l, --load-balance=freq       Frequency of the load balancer. For homogenous\n                                  architectures this can be set to high values,\n                                  for hetereogenous architectures across the\n                                  MPI ranks small values help to equilibrate\n                                  faster. Non-MPI runs are uneffected. Values <\n                                  0 deactivate the load-balancer.\n                                  (default=`500')",
  "      --accepted-load-inbalance=percent\n                                 [0,100] Percent of step time which is ignored\n                                  by load balancer. Low values enable better\n                                  load balancing, but could cause fluctuation\n                                  of polymers.  (default=`8')",
  "      --autotuner-restart-period=period\n                                Period in which the autotuner is restarted.\n                                  (default=`10000')",
  "      --user=user-args          Additional arguments. The usage of these\n                                  arguments defined by the user. The default\n                                  setting ignores the arguments.",
    0
};

static void
init_help_array(void)
{
  som_args_help[0] = som_args_detailed_help[0];
  som_args_help[1] = som_args_detailed_help[1];
  som_args_help[2] = som_args_detailed_help[2];
  som_args_help[3] = som_args_detailed_help[3];
  som_args_help[4] = som_args_detailed_help[4];
  som_args_help[5] = som_args_detailed_help[5];
  som_args_help[6] = som_args_detailed_help[7];
  som_args_help[7] = som_args_detailed_help[8];
  som_args_help[8] = som_args_detailed_help[9];
  som_args_help[9] = som_args_detailed_help[10];
  som_args_help[10] = som_args_detailed_help[11];
  som_args_help[11] = som_args_detailed_help[12];
  som_args_help[12] = som_args_detailed_help[13];
  som_args_help[13] = som_args_detailed_help[14];
  som_args_help[14] = som_args_detailed_help[15];
  som_args_help[15] = som_args_detailed_help[16];
  som_args_help[16] = som_args_detailed_help[17];
  som_args_help[17] = som_args_detailed_help[18];
  som_args_help[18] = som_args_detailed_help[19];
  som_args_help[19] = som_args_detailed_help[20];
  som_args_help[20] = 0;

}

const char *som_args_help[21];

typedef enum {ARG_NO
  , ARG_FLAG
  , ARG_STRING
  , ARG_INT
  , ARG_DOUBLE
  , ARG_ENUM
} cmdline_parser_arg_type;

static
void clear_given (struct som_args *args_info);
static
void clear_args (struct som_args *args_info);

static int
cmdline_parser_internal (int argc, char **argv, struct som_args *args_info,
                        struct cmdline_parser_params *params, const char *additional_error);

static int
cmdline_parser_required2 (struct som_args *args_info, const char *prog_name, const char *additional_error);

const char *cmdline_parser_pseudo_random_number_generator_values[] = {"PCG32", "MT", "TT800", 0}; /*< Possible values for pseudo-random-number-generator. */
const char *cmdline_parser_move_type_values[] = {"TRIAL", "SMART", 0}; /*< Possible values for move-type. */
const char *cmdline_parser_iteration_alg_values[] = {"POLYMER", "SET", 0}; /*< Possible values for iteration-alg. */

static char *
gengetopt_strdup (const char *s);

static
void clear_given (struct som_args *args_info)
{
  args_info->help_given = 0 ;
  args_info->detailed_help_given = 0 ;
  args_info->version_given = 0 ;
  args_info->coord_file_given = 0 ;
  args_info->timesteps_given = 0 ;
  args_info->ana_file_given = 0 ;
  args_info->gpus_given = 0 ;
  args_info->only_gpu_given = 0 ;
  args_info->screen_output_interval_given = 0 ;
  args_info->rng_seed_given = 0 ;
  args_info->pseudo_random_number_generator_given = 0 ;
  args_info->omp_threads_given = 0 ;
  args_info->nonexact_area51_given = 0 ;
  args_info->move_type_given = 0 ;
  args_info->iteration_alg_given = 0 ;
  args_info->skip_tests_given = 0 ;
  args_info->load_balance_given = 0 ;
  args_info->accepted_load_inbalance_given = 0 ;
  args_info->autotuner_restart_period_given = 0 ;
  args_info->user_given = 0 ;
}

static
void clear_args (struct som_args *args_info)
{
  FIX_UNUSED (args_info);
  args_info->coord_file_arg = NULL;
  args_info->coord_file_orig = NULL;
  args_info->timesteps_orig = NULL;
  args_info->ana_file_arg = gengetopt_strdup ("");
  args_info->ana_file_orig = NULL;
  args_info->gpus_arg = 0;
  args_info->gpus_orig = NULL;
  args_info->only_gpu_orig = NULL;
  args_info->screen_output_interval_arg = 10;
  args_info->screen_output_interval_orig = NULL;
  args_info->rng_seed_arg = -1;
  args_info->rng_seed_orig = NULL;
  args_info->pseudo_random_number_generator_arg = pseudo_random_number_generator_arg_PCG32;
  args_info->pseudo_random_number_generator_orig = NULL;
  args_info->omp_threads_arg = 1;
  args_info->omp_threads_orig = NULL;
  args_info->nonexact_area51_flag = 0;
  args_info->move_type_arg = move_type_arg_SMART;
  args_info->move_type_orig = NULL;
  args_info->iteration_alg_arg = iteration_alg_arg_POLYMER;
  args_info->iteration_alg_orig = NULL;
  args_info->skip_tests_flag = 0;
  args_info->load_balance_arg = 500;
  args_info->load_balance_orig = NULL;
  args_info->accepted_load_inbalance_arg = 8;
  args_info->accepted_load_inbalance_orig = NULL;
  args_info->autotuner_restart_period_arg = 10000;
  args_info->autotuner_restart_period_orig = NULL;
  args_info->user_arg = NULL;
  args_info->user_orig = NULL;

}

static
void init_args_info(struct som_args *args_info)
{

  init_help_array();
  args_info->help_help = som_args_detailed_help[0] ;
  args_info->detailed_help_help = som_args_detailed_help[1] ;
  args_info->version_help = som_args_detailed_help[2] ;
  args_info->coord_file_help = som_args_detailed_help[3] ;
  args_info->timesteps_help = som_args_detailed_help[4] ;
  args_info->ana_file_help = som_args_detailed_help[5] ;
  args_info->gpus_help = som_args_detailed_help[7] ;
  args_info->only_gpu_help = som_args_detailed_help[8] ;
  args_info->screen_output_interval_help = som_args_detailed_help[9] ;
  args_info->rng_seed_help = som_args_detailed_help[10] ;
  args_info->pseudo_random_number_generator_help = som_args_detailed_help[11] ;
  args_info->omp_threads_help = som_args_detailed_help[12] ;
  args_info->nonexact_area51_help = som_args_detailed_help[13] ;
  args_info->move_type_help = som_args_detailed_help[14] ;
  args_info->iteration_alg_help = som_args_detailed_help[15] ;
  args_info->skip_tests_help = som_args_detailed_help[16] ;
  args_info->load_balance_help = som_args_detailed_help[17] ;
  args_info->accepted_load_inbalance_help = som_args_detailed_help[18] ;
  args_info->autotuner_restart_period_help = som_args_detailed_help[19] ;
  args_info->user_help = som_args_detailed_help[20] ;

}

void
cmdline_parser_print_version (void)
{
  printf ("%s %s\n",
     (strlen(CMDLINE_PARSER_PACKAGE_NAME) ? CMDLINE_PARSER_PACKAGE_NAME : CMDLINE_PARSER_PACKAGE),
     CMDLINE_PARSER_VERSION);

  if (strlen(som_args_versiontext) > 0)
    printf("\n%s\n", som_args_versiontext);
}

static void print_help_common(void) {
  cmdline_parser_print_version ();

  if (strlen(som_args_purpose) > 0)
    printf("\n%s\n", som_args_purpose);

  if (strlen(som_args_usage) > 0)
    printf("\n%s\n", som_args_usage);

  printf("\n");

  if (strlen(som_args_description) > 0)
    printf("%s\n\n", som_args_description);
}

void
cmdline_parser_print_help (void)
{
  int i = 0;
  print_help_common();
  while (som_args_help[i])
    printf("%s\n", som_args_help[i++]);
}

void
cmdline_parser_print_detailed_help (void)
{
  int i = 0;
  print_help_common();
  while (som_args_detailed_help[i])
    printf("%s\n", som_args_detailed_help[i++]);
}

void
cmdline_parser_init (struct som_args *args_info)
{
  clear_given (args_info);
  clear_args (args_info);
  init_args_info (args_info);
}

void
cmdline_parser_params_init(struct cmdline_parser_params *params)
{
  if (params)
    {
      params->override = 0;
      params->initialize = 1;
      params->check_required = 1;
      params->check_ambiguity = 0;
      params->print_errors = 1;
    }
}

struct cmdline_parser_params *
cmdline_parser_params_create(void)
{
  struct cmdline_parser_params *params =
    (struct cmdline_parser_params *)malloc(sizeof(struct cmdline_parser_params));
  cmdline_parser_params_init(params);
  return params;
}

static void
free_string_field (char **s)
{
  if (*s)
    {
      free (*s);
      *s = 0;
    }
}


static void
cmdline_parser_release (struct som_args *args_info)
{

  free_string_field (&(args_info->coord_file_arg));
  free_string_field (&(args_info->coord_file_orig));
  free_string_field (&(args_info->timesteps_orig));
  free_string_field (&(args_info->ana_file_arg));
  free_string_field (&(args_info->ana_file_orig));
  free_string_field (&(args_info->gpus_orig));
  free_string_field (&(args_info->only_gpu_orig));
  free_string_field (&(args_info->screen_output_interval_orig));
  free_string_field (&(args_info->rng_seed_orig));
  free_string_field (&(args_info->pseudo_random_number_generator_orig));
  free_string_field (&(args_info->omp_threads_orig));
  free_string_field (&(args_info->move_type_orig));
  free_string_field (&(args_info->iteration_alg_orig));
  free_string_field (&(args_info->load_balance_orig));
  free_string_field (&(args_info->accepted_load_inbalance_orig));
  free_string_field (&(args_info->autotuner_restart_period_orig));
  free_string_field (&(args_info->user_arg));
  free_string_field (&(args_info->user_orig));



  clear_given (args_info);
}

/**
 * @param val the value to check
 * @param values the possible values
 * @return the index of the matched value:
 * -1 if no value matched,
 * -2 if more than one value has matched
 */
static int
check_possible_values(const char *val, const char *values[])
{
  int i, found, last;
  size_t len;

  if (!val)   /* otherwise strlen() crashes below */
    return -1; /* -1 means no argument for the option */

  found = last = 0;

  for (i = 0, len = strlen(val); values[i]; ++i)
    {
      if (strncmp(val, values[i], len) == 0)
        {
          ++found;
          last = i;
          if (strlen(values[i]) == len)
            return i; /* exact macth no need to check more */
        }
    }

  if (found == 1) /* one match: OK */
    return last;

  return (found ? -2 : -1); /* return many values or none matched */
}


static void
write_into_file(FILE *outfile, const char *opt, const char *arg, const char *values[])
{
  int found = -1;
  if (arg) {
    if (values) {
      found = check_possible_values(arg, values);
    }
    if (found >= 0)
      fprintf(outfile, "%s=\"%s\" # %s\n", opt, arg, values[found]);
    else
      fprintf(outfile, "%s=\"%s\"\n", opt, arg);
  } else {
    fprintf(outfile, "%s\n", opt);
  }
}


int
cmdline_parser_dump(FILE *outfile, struct som_args *args_info)
{
  int i = 0;

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot dump options to stream\n", CMDLINE_PARSER_PACKAGE);
      return  -1 ;
    }

  if (args_info->help_given)
    write_into_file(outfile, "help", 0, 0 );
  if (args_info->detailed_help_given)
    write_into_file(outfile, "detailed-help", 0, 0 );
  if (args_info->version_given)
    write_into_file(outfile, "version", 0, 0 );
  if (args_info->coord_file_given)
    write_into_file(outfile, "coord-file", args_info->coord_file_orig, 0);
  if (args_info->timesteps_given)
    write_into_file(outfile, "timesteps", args_info->timesteps_orig, 0);
  if (args_info->ana_file_given)
    write_into_file(outfile, "ana-file", args_info->ana_file_orig, 0);
  if (args_info->gpus_given)
    write_into_file(outfile, "gpus", args_info->gpus_orig, 0);
  if (args_info->only_gpu_given)
    write_into_file(outfile, "only-gpu", args_info->only_gpu_orig, 0);
  if (args_info->screen_output_interval_given)
    write_into_file(outfile, "screen-output-interval", args_info->screen_output_interval_orig, 0);
  if (args_info->rng_seed_given)
    write_into_file(outfile, "rng-seed", args_info->rng_seed_orig, 0);
  if (args_info->pseudo_random_number_generator_given)
    write_into_file(outfile, "pseudo-random-number-generator", args_info->pseudo_random_number_generator_orig, cmdline_parser_pseudo_random_number_generator_values);
  if (args_info->omp_threads_given)
    write_into_file(outfile, "omp-threads", args_info->omp_threads_orig, 0);
  if (args_info->nonexact_area51_given)
    write_into_file(outfile, "nonexact-area51", 0, 0 );
  if (args_info->move_type_given)
    write_into_file(outfile, "move-type", args_info->move_type_orig, cmdline_parser_move_type_values);
  if (args_info->iteration_alg_given)
    write_into_file(outfile, "iteration-alg", args_info->iteration_alg_orig, cmdline_parser_iteration_alg_values);
  if (args_info->skip_tests_given)
    write_into_file(outfile, "skip-tests", 0, 0 );
  if (args_info->load_balance_given)
    write_into_file(outfile, "load-balance", args_info->load_balance_orig, 0);
  if (args_info->accepted_load_inbalance_given)
    write_into_file(outfile, "accepted-load-inbalance", args_info->accepted_load_inbalance_orig, 0);
  if (args_info->autotuner_restart_period_given)
    write_into_file(outfile, "autotuner-restart-period", args_info->autotuner_restart_period_orig, 0);
  if (args_info->user_given)
    write_into_file(outfile, "user", args_info->user_orig, 0);


  i =  1 ;
  return i;
}

int
cmdline_parser_file_save(const char *filename, struct som_args *args_info)
{
  FILE *outfile;
  int i = 0;

  outfile = fopen(filename, "w");

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot open file for writing: %s\n", CMDLINE_PARSER_PACKAGE, filename);
      return  -1 ;
    }

  i = cmdline_parser_dump(outfile, args_info);
  fclose (outfile);

  return i;
}

void
cmdline_parser_free (struct som_args *args_info)
{
  cmdline_parser_release (args_info);
}

/** @brief replacement of strdup, which is not standard */
char *
gengetopt_strdup (const char *s)
{
  char *result = 0;
  if (!s)
    return result;

  result = (char*)malloc(strlen(s) + 1);
  if (result == (char*)0)
    return (char*)0;
  strcpy(result, s);
  return result;
}

int
cmdline_parser (int argc, char **argv, struct som_args *args_info)
{
  return cmdline_parser2 (argc, argv, args_info, 0, 1, 1);
}

int
cmdline_parser_ext (int argc, char **argv, struct som_args *args_info,
                   struct cmdline_parser_params *params)
{
  int result;
  result = cmdline_parser_internal (argc, argv, args_info, params, 0);

  if (result ==  -1 )
    {
      cmdline_parser_free (args_info);
      return ( -1 );
    }

  return result;
}

int
cmdline_parser2 (int argc, char **argv, struct som_args *args_info, int override, int initialize, int check_required)
{
  int result;
  struct cmdline_parser_params params;

  params.override = override;
  params.initialize = initialize;
  params.check_required = check_required;
  params.check_ambiguity = 0;
  params.print_errors = 1;

  result = cmdline_parser_internal (argc, argv, args_info, &params, 0);

  if (result ==  -1 )
    {
      cmdline_parser_free (args_info);
      return ( -1 );
    }

  return result;
}

int
cmdline_parser_required (struct som_args *args_info, const char *prog_name)
{
  int result =  1 ;

  if (cmdline_parser_required2(args_info, prog_name, 0) > 0)
    result =  -1 ;

  if (result ==  -1 )
    {
      cmdline_parser_free (args_info);
      return ( -1 );
    }

  return result;
}

int
cmdline_parser_required2 (struct som_args *args_info, const char *prog_name, const char *additional_error)
{
  int error_occurred = 0;
  FIX_UNUSED (additional_error);

  /* checks for required options */
  if (! args_info->coord_file_given)
    {
      fprintf (stderr, "%s: '--coord-file' ('-c') option required%s\n", prog_name, (additional_error ? additional_error : ""));
      error_occurred = 1;
    }

  if (! args_info->timesteps_given)
    {
      fprintf (stderr, "%s: '--timesteps' ('-t') option required%s\n", prog_name, (additional_error ? additional_error : ""));
      error_occurred = 1;
    }


  /* checks for dependences among options */

  return error_occurred;
}


static char *package_name = 0;

/**
 * @brief updates an option
 * @param field the generic pointer to the field to update
 * @param orig_field the pointer to the orig field
 * @param field_given the pointer to the number of occurrence of this option
 * @param prev_given the pointer to the number of occurrence already seen
 * @param value the argument for this option (if null no arg was specified)
 * @param possible_values the possible values for this option (if specified)
 * @param default_value the default value (in case the option only accepts fixed values)
 * @param arg_type the type of this option
 * @param check_ambiguity @see cmdline_parser_params.check_ambiguity
 * @param override @see cmdline_parser_params.override
 * @param no_free whether to free a possible previous value
 * @param multiple_option whether this is a multiple option
 * @param long_opt the corresponding long option
 * @param short_opt the corresponding short option (or '-' if none)
 * @param additional_error possible further error specification
 */
static
int update_arg(void *field, char **orig_field,
               unsigned int *field_given, unsigned int *prev_given,
               char *value, const char *possible_values[],
               const char *default_value,
               cmdline_parser_arg_type arg_type,
               int check_ambiguity, int override,
               int no_free, int multiple_option,
               const char *long_opt, char short_opt,
               const char *additional_error)
{
  char *stop_char = 0;
  const char *val = value;
  int found;
  char **string_field;
  FIX_UNUSED (field);

  stop_char = 0;
  found = 0;

  if (!multiple_option && prev_given && (*prev_given || (check_ambiguity && *field_given)))
    {
      if (short_opt != '-')
        fprintf (stderr, "%s: `--%s' (`-%c') option given more than once%s\n",
               package_name, long_opt, short_opt,
               (additional_error ? additional_error : ""));
      else
        fprintf (stderr, "%s: `--%s' option given more than once%s\n",
               package_name, long_opt,
               (additional_error ? additional_error : ""));
      return 1; /* failure */
    }

  if (possible_values && (found = check_possible_values((value ? value : default_value), possible_values)) < 0)
    {
      if (short_opt != '-')
        fprintf (stderr, "%s: %s argument, \"%s\", for option `--%s' (`-%c')%s\n",
          package_name, (found == -2) ? "ambiguous" : "invalid", value, long_opt, short_opt,
          (additional_error ? additional_error : ""));
      else
        fprintf (stderr, "%s: %s argument, \"%s\", for option `--%s'%s\n",
          package_name, (found == -2) ? "ambiguous" : "invalid", value, long_opt,
          (additional_error ? additional_error : ""));
      return 1; /* failure */
    }

  if (field_given && *field_given && ! override)
    return 0;
  if (prev_given)
    (*prev_given)++;
  if (field_given)
    (*field_given)++;
  if (possible_values)
    val = possible_values[found];

  switch(arg_type) {
  case ARG_FLAG:
    *((int *)field) = !*((int *)field);
    break;
  case ARG_INT:
    if (val) *((int *)field) = strtol (val, &stop_char, 0);
    break;
  case ARG_DOUBLE:
    if (val) *((double *)field) = strtod (val, &stop_char);
    break;
  case ARG_ENUM:
    if (val) *((int *)field) = found;
    break;
  case ARG_STRING:
    if (val) {
      string_field = (char **)field;
      if (!no_free && *string_field)
        free (*string_field); /* free previous string */
      *string_field = gengetopt_strdup (val);
    }
    break;
  default:
    break;
  };

  /* check numeric conversion */
  switch(arg_type) {
  case ARG_INT:
  case ARG_DOUBLE:
    if (val && !(stop_char && *stop_char == '\0')) {
      fprintf(stderr, "%s: invalid numeric value: %s\n", package_name, val);
      return 1; /* failure */
    }
    break;
  default:
    ;
  };

  /* store the original value */
  switch(arg_type) {
  case ARG_NO:
  case ARG_FLAG:
    break;
  default:
    if (value && orig_field) {
      if (no_free) {
        *orig_field = value;
      } else {
        if (*orig_field)
          free (*orig_field); /* free previous string */
        *orig_field = gengetopt_strdup (value);
      }
    }
  };

  return 0; /* OK */
}


int
cmdline_parser_internal (
  int argc, char **argv, struct som_args *args_info,
                        struct cmdline_parser_params *params, const char *additional_error)
{
  int c;	/* Character of the parsed option.  */

  int error_occurred = 0;
  struct som_args local_args_info;

  int override;
  int initialize;
  int check_required;
  int check_ambiguity;

  package_name = argv[0];

  override = params->override;
  initialize = params->initialize;
  check_required = params->check_required;
  check_ambiguity = params->check_ambiguity;

  if (initialize)
    cmdline_parser_init (args_info);

  cmdline_parser_init (&local_args_info);

  optarg = 0;
  optind = 0;
  opterr = params->print_errors;
  optopt = '?';

  while (1)
    {
      int option_index = 0;

      static struct option long_options[] = {
        { "help",	0, NULL, 'h' },
        { "detailed-help",	0, NULL, 0 },
        { "version",	0, NULL, 'V' },
        { "coord-file",	1, NULL, 'c' },
        { "timesteps",	1, NULL, 't' },
        { "ana-file",	1, NULL, 'a' },
        { "gpus",	1, NULL, 'g' },
        { "only-gpu",	1, NULL, 'o' },
        { "screen-output-interval",	1, NULL, 's' },
        { "rng-seed",	1, NULL, 'r' },
        { "pseudo-random-number-generator",	1, NULL, 'p' },
        { "omp-threads",	1, NULL, 'n' },
        { "nonexact-area51",	0, NULL, 0 },
        { "move-type",	1, NULL, 0 },
        { "iteration-alg",	1, NULL, 0 },
        { "skip-tests",	0, NULL, 0 },
        { "load-balance",	1, NULL, 'l' },
        { "accepted-load-inbalance",	1, NULL, 0 },
        { "autotuner-restart-period",	1, NULL, 0 },
        { "user",	1, NULL, 0 },
        { 0,  0, 0, 0 }
      };

      c = getopt_long (argc, argv, "hVc:t:a:g:o:s:r:p:n:l:", long_options, &option_index);

      if (c == -1) break;	/* Exit from `while (1)' loop.  */

      switch (c)
        {
        case 'h':	/* Print help and exit.  */
          cmdline_parser_print_help ();
          cmdline_parser_free (&local_args_info);
          return ( 1 );

        case 'V':	/* Print version and exit.  */
          cmdline_parser_print_version ();
          cmdline_parser_free (&local_args_info);
          return ( 1 );

        case 'c':	/* File containing the system description and all Coordinates. (HDF5-Format).  */


          if (update_arg( (void *)&(args_info->coord_file_arg),
               &(args_info->coord_file_orig), &(args_info->coord_file_given),
              &(local_args_info.coord_file_given), optarg, 0, 0, ARG_STRING,
              check_ambiguity, override, 0, 0,
              "coord-file", 'c',
              additional_error))
            goto failure;

          break;
        case 't':	/* Number of MC sweeps carried out by SOMA..  */


          if (update_arg( (void *)&(args_info->timesteps_arg),
               &(args_info->timesteps_orig), &(args_info->timesteps_given),
              &(local_args_info.timesteps_given), optarg, 0, 0, ARG_INT,
              check_ambiguity, override, 0, 0,
              "timesteps", 't',
              additional_error))
            goto failure;

          break;
        case 'a':	/* File containing the analysis frequency and is going to be appended by the new measured observables. (HDF5-Format).  */


          if (update_arg( (void *)&(args_info->ana_file_arg),
               &(args_info->ana_file_orig), &(args_info->ana_file_given),
              &(local_args_info.ana_file_given), optarg, 0, "", ARG_STRING,
              check_ambiguity, override, 0, 0,
              "ana-file", 'a',
              additional_error))
            goto failure;

          break;
        case 'g':	/* Number of GPUs per MPI-node to use. The devices 0-(gpus-1) are going to be occupied by SOMA. Every node must feature this number of nodes and it is assumed that the scheduler assignes the ranks consequently to the nodes. If set to 0, SOMA tries to run on host-code. Ignored if compiled without OPENACC..  */


          if (update_arg( (void *)&(args_info->gpus_arg),
               &(args_info->gpus_orig), &(args_info->gpus_given),
              &(local_args_info.gpus_given), optarg, 0, "0", ARG_INT,
              check_ambiguity, override, 0, 0,
              "gpus", 'g',
              additional_error))
            goto failure;

          break;
        case 'o':	/* Specify a specific Device for all ranks. Useful for MPI-single rank runs. This option overrides the --gpus option..  */


          if (update_arg( (void *)&(args_info->only_gpu_arg),
               &(args_info->only_gpu_orig), &(args_info->only_gpu_given),
              &(local_args_info.only_gpu_given), optarg, 0, 0, ARG_INT,
              check_ambiguity, override, 0, 0,
              "only-gpu", 'o',
              additional_error))
            goto failure;

          break;
        case 's':	/* Specify the number of seconds between an output about timings on the screen..  */


          if (update_arg( (void *)&(args_info->screen_output_interval_arg),
               &(args_info->screen_output_interval_orig), &(args_info->screen_output_interval_given),
              &(local_args_info.screen_output_interval_given), optarg, 0, "10", ARG_DOUBLE,
              check_ambiguity, override, 0, 0,
              "screen-output-interval", 's',
              additional_error))
            goto failure;

          break;
        case 'r':	/* Global seed for the pseudo-random number generator. If you pass seed < 0, seed = time(NULL) will be used. Option useful for debuggin purposes..  */


          if (update_arg( (void *)&(args_info->rng_seed_arg),
               &(args_info->rng_seed_orig), &(args_info->rng_seed_given),
              &(local_args_info.rng_seed_given), optarg, 0, "-1", ARG_INT,
              check_ambiguity, override, 0, 0,
              "rng-seed", 'r',
              additional_error))
            goto failure;

          break;
        case 'p':	/* Option to select the pseudo random number generator..  */


          if (update_arg( (void *)&(args_info->pseudo_random_number_generator_arg),
               &(args_info->pseudo_random_number_generator_orig), &(args_info->pseudo_random_number_generator_given),
              &(local_args_info.pseudo_random_number_generator_given), optarg, cmdline_parser_pseudo_random_number_generator_values, "PCG32", ARG_ENUM,
              check_ambiguity, override, 0, 0,
              "pseudo-random-number-generator", 'p',
              additional_error))
            goto failure;

          break;
        case 'n':	/* Number of omp threads used per MPI rank. If you pass n < 1 it will be set to 1..  */


          if (update_arg( (void *)&(args_info->omp_threads_arg),
               &(args_info->omp_threads_orig), &(args_info->omp_threads_given),
              &(local_args_info.omp_threads_given), optarg, 0, "1", ARG_INT,
              check_ambiguity, override, 0, 0,
              "omp-threads", 'n',
              additional_error))
            goto failure;

          break;
        case 'l':	/* Frequency of the load balancer. For homogenous architectures this can be set to high values, for hetereogenous architectures across the MPI ranks small values help to equilibrate faster. Non-MPI runs are uneffected. Values < 0 deactivate the load-balancer..  */


          if (update_arg( (void *)&(args_info->load_balance_arg),
               &(args_info->load_balance_orig), &(args_info->load_balance_given),
              &(local_args_info.load_balance_given), optarg, 0, "500", ARG_INT,
              check_ambiguity, override, 0, 0,
              "load-balance", 'l',
              additional_error))
            goto failure;

          break;

        case 0:	/* Long option with no short option */
          if (strcmp (long_options[option_index].name, "detailed-help") == 0) {
            cmdline_parser_print_detailed_help ();
            cmdline_parser_free (&local_args_info);
            return ( 1 );
          }

          /* Specify to use the exact check of area51. This includes checks, whether a particle moves through a forbidden area51. Performance might be slightly increased if switched to nonexact. Configuration generation is always in exact mode..  */
          if (strcmp (long_options[option_index].name, "nonexact-area51") == 0)
          {


            if (update_arg((void *)&(args_info->nonexact_area51_flag), 0, &(args_info->nonexact_area51_given),
                &(local_args_info.nonexact_area51_given), optarg, 0, 0, ARG_FLAG,
                check_ambiguity, override, 1, 0, "nonexact-area51", '-',
                additional_error))
              goto failure;

          }
          /* Specify the Monte-Carlo move type..  */
          else if (strcmp (long_options[option_index].name, "move-type") == 0)
          {


            if (update_arg( (void *)&(args_info->move_type_arg),
                 &(args_info->move_type_orig), &(args_info->move_type_given),
                &(local_args_info.move_type_given), optarg, cmdline_parser_move_type_values, "SMART", ARG_ENUM,
                check_ambiguity, override, 0, 0,
                "move-type", '-',
                additional_error))
              goto failure;

          }
          /* Specify the iteration algorithm of the beads. This specifies also the level of parallelism that is possible..  */
          else if (strcmp (long_options[option_index].name, "iteration-alg") == 0)
          {


            if (update_arg( (void *)&(args_info->iteration_alg_arg),
                 &(args_info->iteration_alg_orig), &(args_info->iteration_alg_given),
                &(local_args_info.iteration_alg_given), optarg, cmdline_parser_iteration_alg_values, "POLYMER", ARG_ENUM,
                check_ambiguity, override, 0, 0,
                "iteration-alg", '-',
                additional_error))
              goto failure;

          }
          /* Skip tests SOMA is usually preforming before and after the simulation to ensure integrety of the data..  */
          else if (strcmp (long_options[option_index].name, "skip-tests") == 0)
          {


            if (update_arg((void *)&(args_info->skip_tests_flag), 0, &(args_info->skip_tests_given),
                &(local_args_info.skip_tests_given), optarg, 0, 0, ARG_FLAG,
                check_ambiguity, override, 1, 0, "skip-tests", '-',
                additional_error))
              goto failure;

          }
          /*  [0,100] Percent of step time which is ignored by load balancer. Low values enable better load balancing, but could cause fluctuation of polymers..  */
          else if (strcmp (long_options[option_index].name, "accepted-load-inbalance") == 0)
          {


            if (update_arg( (void *)&(args_info->accepted_load_inbalance_arg),
                 &(args_info->accepted_load_inbalance_orig), &(args_info->accepted_load_inbalance_given),
                &(local_args_info.accepted_load_inbalance_given), optarg, 0, "8", ARG_DOUBLE,
                check_ambiguity, override, 0, 0,
                "accepted-load-inbalance", '-',
                additional_error))
              goto failure;

          }
          /* Period in which the autotuner is restarted..  */
          else if (strcmp (long_options[option_index].name, "autotuner-restart-period") == 0)
          {


            if (update_arg( (void *)&(args_info->autotuner_restart_period_arg),
                 &(args_info->autotuner_restart_period_orig), &(args_info->autotuner_restart_period_given),
                &(local_args_info.autotuner_restart_period_given), optarg, 0, "10000", ARG_INT,
                check_ambiguity, override, 0, 0,
                "autotuner-restart-period", '-',
                additional_error))
              goto failure;

          }
          /* Additional arguments. The usage of these arguments defined by the user. The default setting ignores the arguments..  */
          else if (strcmp (long_options[option_index].name, "user") == 0)
          {


            if (update_arg( (void *)&(args_info->user_arg),
                 &(args_info->user_orig), &(args_info->user_given),
                &(local_args_info.user_given), optarg, 0, 0, ARG_STRING,
                check_ambiguity, override, 0, 0,
                "user", '-',
                additional_error))
              goto failure;

          }

          break;
        case '?':	/* Invalid option.  */
          /* `getopt_long' already printed an error message.  */
          goto failure;

        default:	/* bug: option not considered.  */
          fprintf (stderr, "%s: option unknown: %c%s\n", CMDLINE_PARSER_PACKAGE, c, (additional_error ? additional_error : ""));
          abort ();
        } /* switch */
    } /* while */



  if (check_required)
    {
      error_occurred += cmdline_parser_required2 (args_info, argv[0], additional_error);
    }

  cmdline_parser_release (&local_args_info);

  if ( error_occurred )
    return ( -1 );

  return 0;

failure:

  cmdline_parser_release (&local_args_info);
  return ( -1 );
}
